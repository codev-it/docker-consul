# Codev-IT
# docker env builder
#

# default
NAME="Codev-IT"
EMAIL="office@codev-it.at"

# docker info
DOCKER=docker
DOCKER_ID=codevit
DOCKER_NAME=consul
DOCKER_TAG=latest
DOCKER_FILE=Dockerfile
DOCKER_SHELL=/bin/bash

.PHONY: help build test push shell run start stop logs clean release

help:
	@echo "Please use \`make <target>' where <target> is one of"
	@echo "  build	     		build docker image"
	@echo "  build-all	   		build all folders"
	@echo "  push		   		push to docker hub"
	@echo "  push-all	   		push all to docker hub"
	@echo "  shell    	   		start bash session"
	@echo "  run		   		run container"
	@echo "  - CMD=val    		docker container command"
	@echo "  detach		   		run container in detach mode"
	@echo "  detach-all	   		run all container in detach mode"
	@echo "  remove		   		remove container"
	@echo "  remove-all	   		remove all container"
	@echo "  logs		   		get logs"
	@echo "  logs-all	   		get all logs"

build:
	@$(DOCKER) build \
		-t "$$(echo "${DOCKER_ID}/${DOCKER_NAME}:${DOCKER_TAG}" \
			| tr '[:upper:]' '[:lower:]')" \
			--build-arg BUILD_DATE=`date -u +"%Y-%m-%dT%H:%M:%SZ"` \
			--build-arg VCS_REF=`git rev-parse --short HEAD` \
			--build-arg VERSION="${DOCKER_TAG}" \
			"${PWD}";

build-all:
	@for build in ${PWD}/*; do \
		if [ -f "$$build/Dockerfile" ]; then \
			make build DOCKER_NAME="$${build##*/}" PWD="$${PWD}/$${build##*/}"; \
		fi \
	done

push:
	@$(DOCKER) push ${DOCKER_ID}/${DOCKER_NAME}:${DOCKER_TAG}

push-all:
	@for build in ${PWD}/*; do \
		if [ -f "$$build/Dockerfile" ]; then \
			make push DOCKER_NAME="$${build##*/}"; \
		fi \
	done

shell:
	@$(DOCKER) run --rm --name ${DOCKER_NAME} -i -t $(OPTS) $(PORTS) $(DNS) $(VOLUMES) $(ENV) ${DOCKER_ID}/${DOCKER_NAME}:${DOCKER_TAG} ${DOCKER_SHELL}

run:
	@$(DOCKER) run --rm --name ${DOCKER_NAME} -e DEBUG=1 $(OPTS) $(PORTS) $(DNS) $(VOLUMES) $(ENV) ${DOCKER_ID}/${DOCKER_NAME}:${DOCKER_TAG} $(CMD)

detach:
	@$(DOCKER) run -id --rm --name ${DOCKER_NAME} $(PARMS) $(OPTS) $(DNS) $(VOLUMES) $(ENV) ${DOCKER_ID}/${DOCKER_NAME}:${DOCKER_TAG} $(CMD)

detach-all:
	@for build in ${PWD}/*; do \
		if [ -f "$$build/Dockerfile" ]; then \
			make detach DOCKER_NAME="$${build##*/}"; \
		fi \
	done

stop:
	@$(DOCKER) stop ${DOCKER_NAME}

exec:
	@$(DOCKER) exec -i ${DOCKER_NAME} $(CMD)

exec-all:
	@for build in ${PWD}/*; do \
		if [ -f "$$build/Dockerfile" ]; then \
			make exec DOCKER_NAME="$${build##*/}"; \
		fi \
	done

logs:
	@$(DOCKER) logs $(PARMS) ${DOCKER_NAME}

logs-all:
	@for build in ${PWD}/*; do \
		if [ -f "$$build/Dockerfile" ]; then \
			make logs DOCKER_NAME="$${build##*/}"; \
		fi \
	done

remove:
	@$(DOCKER) rm -f ${DOCKER_NAME}

remove-all:
	@for build in ${PWD}/*; do \
		if [ -f "$$build/Dockerfile" ]; then \
			make remove DOCKER_NAME="$${build##*/}"; \
		fi \
	done

release: build push

# tools
tool-portainer:
	@$(DOCKER) volume create portainer_data
	@$(DOCKER) run -d -p 9000:9000 --name portainer --restart always \
		-v /var/run/docker.sock:/var/run/docker.sock \
		-v portainer_data:/data portainer/portainer

tool-net:
	@$(DOCKER) run --rm --name tool-net -it amouat/network-utils

tool-conainer-ip:
	@$(DOCKER) inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' ${NAME}

# test shortcuts
TEST_PARAMS=\
	PORTS="-p 8500:8500 -p 127.17.0.1:53:53/udp" \
	ENV="--env-file .env" \
	VOLUMES=" \
		-v ${PWD}/test/test.sh:/test.sh \
		-v ${PWD}/test/config/server-config.json:/consul/config/server.json \
		-v /var/run/docker.sock:/var/run/docker.sock" \
	CMD="agent -server -bootstrap -dev"

utest:
	@echo "------ Process overview. ------"
	@$(DOCKER) ps
	@if [ -z `$(DOCKER) exec ${DOCKER_NAME} test -f /healthcheck || echo no` ]; then \
		echo "------ Health-Check test script. ------"; \
		$(DOCKER) exec -i ${DOCKER_NAME} /healthcheck; \
	fi
	@if [ -z `$(DOCKER) exec ${DOCKER_NAME} test -f /test.sh || echo no` ]; then \
		echo "------ UTest test script. ------"; \
		$(DOCKER) exec -i ${DOCKER_NAME} sh /test.sh; \
	fi

test-run: build
	@make run $(TEST_PARAMS)

test-detach: build
	@make detach $(TEST_PARAMS)

test-composer-up:
	cd test && docker-compose up -d

test-composer-rm:
	cd test && docker-compose rm

test-stack-up:
	@$(DOCKER) stack deploy -c ./test/docker-compose.yml ${DOCKER_NAME}

test-stack-rm:
	@$(DOCKER) stack rm ${DOCKER_NAME}

test-cluster-init:
	@bash test/cluster.sh create ${DOCKER_NAME} ${M} ${N}

test-cluster-up:
	@bash test/cluster.sh deploy ${DOCKER_NAME}

test-cluster-rm:
	@bash test/cluster.sh remove ${DOCKER_NAME}