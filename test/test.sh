#!/bin/sh

set -eux

# defaults
DNS_PORT=${CONSUL_DNS_PORT:-53}

# install test tools
apk add bind-tools

# create test service
consul services register -id=test -name=test -address=10.0.0.1 -port=80

# config default acl rules
consul acl policy create -name 'list-all-nodes' -rules 'node_prefix "" { policy = "read" }'
consul acl token update -id 00000000-0000-0000-0000-000000000002 -policy-name list-all-nodes -description "Anonymous Token - Can List Nodes"
consul acl policy create -name 'service-consul-read' -rules 'service "consul" { policy = "read" }'
consul acl token update -id 00000000-0000-0000-0000-000000000002 --merge-policies -description "Anonymous Token - Can List Nodes" -policy-name service-consul-read

# test dns by google
dig @127.0.0.1 -p ${DNS_PORT} google.com. ANY

# test consul DNS
dig @127.0.0.1 -p ${DNS_PORT} consul.service.consul. ANY

# test services acl
consul acl policy create -name 'service-test-read' -rules 'service "test" { policy = "read" }'
consul acl token update -id 00000000-0000-0000-0000-000000000002 --merge-policies -description "Anonymous Token - Can List Nodes" -policy-name service-test-read

# test services dns
dig @127.0.0.1 -p ${DNS_PORT} test.service.consul. ANY
