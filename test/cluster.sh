#!/bin/bash

set -eux

if [[ -x "$(whereis docker-machine | cut -d':' -f'2')" ]]; then
  echo "No Docker Machine on the host system available."
  exit 1
fi

COMMAND=${1-'create'}
NAME=${2-'cluster'}
MANAGERS=${3-1}
NODES=${4-1}
PREFIX=${5-'codevit'}

function cluster_node_add() {
  name="${1}"
  token=${2}
  docker-machine create -d virtualbox ${name}
  docker-machine ssh ${name} "docker swarm join \
    --token='${token}' \
    --listen-addr $(docker-machine ip ${name}) \
    --advertise-addr $(docker-machine ip ${name}) \
    $(docker-machine ip ${NAME}-manager)"
  eval $(docker-machine env ${name})
  docker build -t "${PREFIX}/${NAME}:latest" .
}

function cluster_create() {
  docker-machine create -d virtualbox ${NAME}-manager
  docker-machine ssh ${NAME}-manager "docker swarm init \
    --listen-addr $(docker-machine ip ${NAME}-manager) \
    --advertise-addr $(docker-machine ip ${NAME}-manager)"
  eval $(docker-machine env consul-manager)
  docker build -t "${PREFIX}/${NAME}:latest" .

  manager_token=$(docker-machine ssh ${NAME}-manager "docker swarm \
    join-token manager -q")
  worker_token=$(docker-machine ssh ${NAME}-manager "docker swarm \
    join-token worker -q")

  if [[ ${MANAGERS} -gt 1 ]]; then
    for (( c=1; c<${MANAGERS}; c++ )); do
      id=$(expr ${c} + 1)
      cluster_node_add "${NAME}-manager${id}" "${manager_token}"
    done
  fi

  if [[ ${NODES} != 0 ]]; then
    docker-machine create -d virtualbox ${NAME}-node
    docker-machine ssh ${NAME}-node "docker swarm join \
      --token=${worker_token} \
      --listen-addr $(docker-machine ip ${NAME}-node) \
      --advertise-addr $(docker-machine ip ${NAME}-node) \
      $(docker-machine ip ${NAME}-manager)"
    eval $(docker-machine env consul-node)
    docker build -t "${PREFIX}/${NAME}:latest" .
  fi

  if [[ ${NODES} -gt 1 ]]; then
    for (( c=1; c<${NODES}; c++ )); do
      id=$(expr ${c} + 1)
      cluster_node_add "${NAME}-node${id}" "${worker_token}"
    done
  fi
}

function cluster_remove() {
  vms=$(docker-machine ls \
    | grep -e "${NAME}-manager" -e "${NAME}-node" \
    | cut -d' ' -f'1' \
    | tr '\n' ' ')
    if [[ -n ${vms} ]]; then
      docker-machine stop ${vms}
      docker-machine rm ${vms}
    fi
}

function cluster_deploy_stack() {
  eval $(docker-machine env consul-manager)
  docker build -t "${PREFIX}/${NAME}:latest" .
  docker stack deploy -c test/docker-compose.yml ${NAME}
}

case ${COMMAND} in
create)
  cluster_create
  cluster_deploy_stack
  ;;
deploy)
  cluster_deploy_stack
  ;;
remove)
  cluster_remove
  ;;
esac
