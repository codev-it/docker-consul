#!/bin/sh

# debug
if [ "${DEBUG}" = true ]; then
  set -eux
else
  set -e
fi

if [ "$(ps aux | pgrep -n consul)" != "" ]; then
  health_state=$(consul info | awk '/health_score/{print $3}' | tr -d '\n')
  if [ -n "${CONSUL_HTTP_TOKEN}" ]; then
    consul_http=$(curl -s --header "X-Consul-Token: ${CONSUL_HTTP_TOKEN}" http://localhost:8500/)
  else
    consul_http=$(curl -s http://localhost:8500/)
  fi
  if [ "${health_state}" -le 0 ] && [ -n "${consul_http}" ]; then
    echo "success"
    exit 0
  fi
fi
echo "unhealthy"
exit 1