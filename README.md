# Docker Consul

Docker Consul fork with more functionality.

##### Image tag info

Please note that the image with the tag "latest" uses the source code of the master branch.
We strongly recommend using images with stability tags only.

## Extensions

  * Health check
  * MySQL client

## Config

This image implements the full functionality of the main Consul Docker build.

##### Overview of the documentations:

  * Consul Docker Build: https://hub.docker.com/_/consul
  * Consul: https://www.consul.io/docs
  * Consul API: https://www.consul.io/api

#### Test example

  * https://bitbucket.org/codev-it/docker-consul/src/master/test/